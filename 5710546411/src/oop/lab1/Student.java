package oop.lab1;

/**
 * A more complex but still simple model for a person with a name and id.
 * 
 * @author Sakara Somapa
 */
public class Student extends Person {
	/** the student's id. may contain only numerals. */
	private long id;
	
	/**
	 * Initialize a new Student object that inherit from Person.
	 * @param name is the name of the new Person
	 * @param id is the id of the new Person
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	/**
	 * Compare student's by name.
	 * They are equal if the name matches.
	 * @param other is another Student to compare to this one.
	 * @return true if the name is same, false otherwise.
	 */
	public boolean equals(Object other) {
		if (other == null) return false;
		if ( other.getClass() != this.getClass() ) return false;
		return (id == (((Student) other).getId()));
	}

	private long getId() {
		return id;
	}
}
